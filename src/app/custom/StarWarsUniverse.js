import Starship from './Starship';

export default class StarWarsUniverse {
  constructor() {
    this.starships = [];
  }
  async init() {
    const starshipCount = await this._getStarshipCount();

    const startShip = await this._createStarShips(starshipCount);

    const validShip = this._validateData(startShip);

    this.createAllStarshipClassInstances(validShip);
  }

  _getStarshipCount() {
    return fetch('https://swapi.booost.bg/api/starships/').then((response) => response.json()).then((data) => data.count);
  }

  get theBestStarship() {
    this.starships.sort((a, b) => a.maxDaysInSpace - b.maxDaysInSpace);

    return this.starships[this.starships.length - 1];
  }

  async _createStarShips(number) {
    const data = [];

    for (let i = 1; i < number; i++) {
      let starshipDataAPI = await fetch(`https://swapi.booost.bg/api/starships/${i}`);
      if (starshipDataAPI.ok) {
        let starshipDataAPI = await fetch(`https://swapi.booost.bg/api/starships/${i}`).then((response) => response.json());
        data.push(starshipDataAPI);
      } else {
        continue;
      }
    }
    for (let i = 0; i < data.length; i++) {
      const element = data[i];

      element.id = i + 1;
    }

    return data;
  }

  _validateData(ships) {
    const validatedShips = ships.filter((el) => {
      if (
        !((el.consumables === 'unknown'
          || el.consumables === null
          || el.consumables === undefined)
          || (el.passengers === undefined
            || el.passengers === null
            || el.passengers === 'n/a'
            || el.passengers === '0'
            || el.passengers === 'unknown'
          ))
      ) {
        if (el.passengers.includes(',')) {
          el.passengers = el.passengers.replace(',', '.');
        }
        el.passengers = Number(el.passengers);
        el.consumables = this.formatData(el.consumables);

        return el;
      }
    });

    return validatedShips;
  }

  createAllStarshipClassInstances(approvedShips) {
    for (let i = 0; i < approvedShips.length; i++) {
      const ship = new Starship(approvedShips[i].name, approvedShips[i].consumables, approvedShips[i].passengers);

      this.starships.push(ship);
    }
  }

  formatData(oneShip) {
    oneShip = oneShip.split(' ');

    const days = Number(oneShip[0]);

    switch (oneShip[1]) {
      case 'year':
        oneShip = days * 365;
        break;
      case 'years':
        oneShip = days * 365;
        break;
      case 'month':
        oneShip = days * 30;
        break;
      case 'months':
        oneShip = days * 30;
        break;
      case 'week':
        oneShip = days * 7;
        break;
      case 'weeks':
        oneShip = days * 7;
        break;
      default:
        oneShip = days;
        break;
    }

    return oneShip;
  }
}
